package com.example.kweather

import com.example.kweather.di.SchedulerProvider
import io.reactivex.schedulers.Schedulers

class TestScheduler : SchedulerProvider {
    override fun ui() = Schedulers.trampoline()
    override fun io() = Schedulers.trampoline()
}