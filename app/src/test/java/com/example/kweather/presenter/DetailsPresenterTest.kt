package com.example.kweather.presenter

import com.example.kweather.TestScheduler
import com.example.kweather.di.SchedulerProvider
import com.example.kweather.model.DataStore
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity
import com.example.kweather.view.detail.DetailsView
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class DetailsPresenterTest {

    @Mock private lateinit var view: DetailsView
    @Mock private lateinit var dataStore: DataStore
    @Mock private lateinit var disposable: CompositeDisposable

    private lateinit var presenter: DetailsPresenter
    private val scheduler: SchedulerProvider = TestScheduler()
    private val cityId = 1
    private val city = CityDbEntity(1, "city", "country")
    private val weather = WeatherDbEntity(1, 1, 1f, 1f, 1, "", "", "", 1)
    private val weatherList = listOf(weather)
    private val pair = Pair(city, weatherList)
    private val throwable = Throwable()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = DetailsPresenter(dataStore, scheduler)
        presenter.init(view, disposable)
    }

    @Test
    fun testRequestWeatherForCitySuccess() {
        val s = Single.just(pair)
        whenever(dataStore.getWeatherFromTodayForCity(cityId)).thenReturn(s)
        val test = dataStore.getWeatherFromTodayForCity(cityId).test()
        presenter.requestWeatherForCity(cityId)
        test.assertNoErrors()
        test.assertComplete()
        verify(view, times(1)).updateData(pair)
        verify(view, times(1)).hideProgress()
        verifyNoMoreInteractions(view)
    }

    @Test
    fun testRequestWeatherForCityFailure() {
        val s = Single.error<Pair<CityDbEntity, List<WeatherDbEntity>>>(throwable)
        whenever(dataStore.getWeatherFromTodayForCity(cityId)).thenReturn(s)
        val test = dataStore.getWeatherFromTodayForCity(cityId).test()
        presenter.requestWeatherForCity(cityId)
        test.assertError(throwable)
        verify(view, times(1)).showMessage(any())
        verifyNoMoreInteractions(view)
    }

}