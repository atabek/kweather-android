package com.example.kweather.presenter

import com.example.kweather.TestScheduler
import com.example.kweather.di.SchedulerProvider
import com.example.kweather.model.DataStore
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity
import com.example.kweather.view.main.MainView
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MainPresenterTest {
    @Mock private lateinit var view: MainView
    @Mock private lateinit var dataStore: DataStore
    @Mock private lateinit var disposable: CompositeDisposable

    private lateinit var presenter: MainPresenter
    private val scheduler: SchedulerProvider = TestScheduler()
    private val city = CityDbEntity(1, "city", "country")
    private val weather = WeatherDbEntity(1, 1, 1f, 1f, 1, "", "", "", 1)
    private val pair = Pair(hashMapOf(Pair(1, city)), hashMapOf(Pair(1, weather)))
    private val result = listOf(Pair(city, weather))
    private val throwable = Throwable()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MainPresenter(dataStore, scheduler)
        presenter.init(view, disposable)
    }

    @Test
    fun testRequestTodaysWeatherForCitiesSuccess() {
        val s = Single.just(pair)
        whenever(dataStore.getCombinedWeatherForToday()).thenReturn(s)
        val test = dataStore.getCombinedWeatherForToday().test()
        presenter.requestTodaysWeatherForCities()
        test.assertNoErrors()
        test.assertComplete()
        verify(view, times(1)).updateCities(result)
        verifyNoMoreInteractions(view)
    }

    @Test
    fun testRequestTodaysWeatherForCitiesFailure() {
        val s = Single.error<Pair<HashMap<Int, CityDbEntity>, HashMap<Int, WeatherDbEntity>>>(throwable)
        whenever(dataStore.getCombinedWeatherForToday()).thenReturn(s)
        val test = dataStore.getCombinedWeatherForToday().test()
        presenter.requestTodaysWeatherForCities()
        test.assertError(throwable)
        verify(view, times(1)).showMessage(any())
        verifyNoMoreInteractions(view)
    }
}