package com.example.kweather.di.module

import com.example.kweather.KweatherApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: KweatherApp) {

    @Singleton
    @Provides
    fun providesApp() = app

    @Singleton
    @Provides
    fun providesContext() = app.applicationContext

    @Singleton
    @Provides
    fun providesResources() = app.resources

}