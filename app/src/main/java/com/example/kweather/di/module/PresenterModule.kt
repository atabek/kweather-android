package com.example.kweather.di.module

import android.content.res.Resources
import com.example.kweather.di.AppScheduler
import com.example.kweather.di.SchedulerProvider
import com.example.kweather.model.DataStore
import com.example.kweather.presenter.DetailsPresenter
import com.example.kweather.presenter.MainPresenter
import com.example.kweather.presenter.SearchCityPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PresenterModule {

    @Provides
    @Singleton
    fun providesSearchCityPresenter(resources: Resources, dataStore: DataStore, scheduler: SchedulerProvider)
        = SearchCityPresenter(resources, dataStore, scheduler)

    @Provides
    @Singleton
    fun providesMainPresenter(dataStore: DataStore, scheduler: SchedulerProvider) = MainPresenter(dataStore, scheduler)

    @Provides
    @Singleton
    fun providesDetailsPresenter(dataStore: DataStore, scheduler: SchedulerProvider) = DetailsPresenter(dataStore, scheduler)

    @Provides
    @Singleton
    fun providesSchedules(): SchedulerProvider = AppScheduler()
}