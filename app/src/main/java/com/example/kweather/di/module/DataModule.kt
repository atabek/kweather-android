package com.example.kweather.di.module

import android.arch.persistence.room.Room
import android.content.Context
import com.example.kweather.model.DataStore
import com.example.kweather.model.db.Database
import com.example.kweather.model.network.OpenWeatherMapService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun providesDatabase(context: Context) = Room.databaseBuilder(context, Database::class.java, "kweather.db").build()

    @Provides
    @Singleton
    fun providesDataStore(networkClient: OpenWeatherMapService, dbClient: Database) = DataStore(networkClient, dbClient)
}