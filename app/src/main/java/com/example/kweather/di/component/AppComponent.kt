package com.example.kweather.di.component

import com.example.kweather.KweatherApp
import com.example.kweather.di.module.AppModule
import com.example.kweather.di.module.DataModule
import com.example.kweather.di.module.NetworkModule
import com.example.kweather.di.module.PresenterModule
import com.example.kweather.view.detail.DetailsActivity
import com.example.kweather.view.main.MainActivity
import com.example.kweather.view.search.SearchCityActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, PresenterModule::class, NetworkModule::class, DataModule::class))
interface AppComponent {
    fun inject(app: KweatherApp)
    fun inject(activity: MainActivity)
    fun inject(activity: SearchCityActivity)
    fun inject(activity: DetailsActivity)
}