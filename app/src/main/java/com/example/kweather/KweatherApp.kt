package com.example.kweather

import android.app.Application
import com.example.kweather.di.component.AppComponent
import com.example.kweather.di.component.DaggerAppComponent
import com.example.kweather.di.module.AppModule

class KweatherApp : Application() {
    val component: AppComponent by lazy {
        DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}