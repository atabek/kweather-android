package com.example.kweather.presenter

import com.example.kweather.R
import com.example.kweather.di.SchedulerProvider
import com.example.kweather.model.DataStore
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity
import com.example.kweather.view.main.MainView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class MainPresenter(private val dataStore: DataStore, private val scheduler: SchedulerProvider) {
    private lateinit var view: MainView
    private lateinit var disposable: CompositeDisposable

    fun init(view: MainView, disposable: CompositeDisposable) {
        this.view = view
        this.disposable = disposable
    }

    fun terminate() {
        disposable.dispose()
    }

    fun requestTodaysWeatherForCities() {
        dataStore.getCombinedWeatherForToday()
            .observeOn(scheduler.ui())
            .subscribeOn(scheduler.io())
            .subscribe({
                var cityWeatherPair: MutableList<Pair<CityDbEntity?, WeatherDbEntity?>> = ArrayList()
                for (cityId in it.first.keys) {
                    cityWeatherPair.add(Pair(it.first[cityId], it.second[cityId]))
                }
                cityWeatherPair = cityWeatherPair.sortedWith(compareBy({ it.first?.nameWithCountry() })).toMutableList()
                view.updateCities(cityWeatherPair)
            }, {
                view.showMessage(R.string.couldnt_retrieve_cities)
            })
            .addTo(disposable)
    }
}