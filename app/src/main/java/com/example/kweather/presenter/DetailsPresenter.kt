package com.example.kweather.presenter

import com.example.kweather.R
import com.example.kweather.di.SchedulerProvider
import com.example.kweather.model.DataStore
import com.example.kweather.view.detail.DetailsView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class DetailsPresenter(private val dataStore: DataStore, private val scheduler: SchedulerProvider) {
    private lateinit var view: DetailsView
    private lateinit var disposable: CompositeDisposable

    fun init(view: DetailsView, disposable: CompositeDisposable) {
        this.view = view
        this.disposable = disposable
    }

    fun terminate() {
        disposable.dispose()
    }

    fun requestWeatherForCity(cityId: Int) {
        dataStore
            .getWeatherFromTodayForCity(cityId)
            .observeOn(scheduler.ui())
            .subscribeOn(scheduler.io())
            .subscribe({
                view.updateData(it)
                view.hideProgress()
            }, {
                view.showMessage(R.string.couldnt_retrieve_weather)
            })
            .addTo(disposable)
    }

    fun refreshData(cityId: Int) {
        dataStore
            .getWeatherFromTodayForCity(cityId)
            .observeOn(scheduler.ui())
            .subscribeOn(scheduler.io())
            .subscribe({
                if (it.second.size == 7) {
                    view.showMessage(R.string.no_need_for_update)
                    view.hideProgress()
                } else {
                    forceRefresh(it.first.name)
                }
            }, {
                view.showMessage(R.string.couldnt_retrieve_weather)
                view.hideProgress()
            })
            .addTo(disposable)
    }

    private fun forceRefresh(city: String) {
        dataStore
            .requestWeatherForCity(city)
            .observeOn(scheduler.ui())
            .subscribeOn(scheduler.io())
            .subscribe({
                requestWeatherForCity(it.city.id)
            }, {
                view.showMessage(R.string.couldnt_retrieve_weather)
                view.hideProgress()
            })
            .addTo(disposable)

    }
}