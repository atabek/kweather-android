package com.example.kweather.presenter

import android.content.res.Resources
import com.example.kweather.R
import com.example.kweather.di.SchedulerProvider
import com.example.kweather.model.DataStore
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.network.entity.CityApiEntity
import com.example.kweather.view.search.SearchCityView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class SearchCityPresenter(private val resources: Resources, private val dataStore: DataStore,
    private val scheduler: SchedulerProvider) {
    private lateinit var view: SearchCityView
    private lateinit var disposable: CompositeDisposable

    fun init(view: SearchCityView, disposable: CompositeDisposable) {
        this.view = view
        this.disposable = disposable
    }

    fun terminate() {
        disposable.dispose()
    }

    fun getSelectedCities() {
        dataStore
            .getAllCities()
            .observeOn(scheduler.ui())
            .subscribeOn(scheduler.io())
            .subscribe({
                view.updateCities(it)
                view.hideKeyboard()
            }, {
                view.showMessage(resources.getString(R.string.couldnt_retrieve_cities))
                view.hideKeyboard()
            })
            .addTo(disposable)
    }

    fun searchCity(city: String) {
        view.showProgressBar()
        dataStore.isCityExist(city)
            .observeOn(scheduler.ui())
            .subscribeOn(scheduler.io())
            .subscribe({
                view.showMessage(resources.getString(R.string.already_in_list, city))
                view.hideKeyboard()
                view.hideProgressBar()
            }, {
                // city was not added before, thus we need to make a request, and then save the city
                sendRequestForCity(city)
            })
            .addTo(disposable)
    }

    private fun sendRequestForCity(city: String) {
        dataStore.requestWeatherForCity(city)
            .observeOn(scheduler.ui())
            .subscribeOn(scheduler.io())
            .subscribe(
                {
                    saveCityAsSelected(it.city)
                }, {
                view.showMessage(resources.getString(R.string.try_again_later))
                view.hideKeyboard()
                view.hideProgressBar()
            })
            .addTo(disposable)

    }

    private fun saveCityAsSelected(city: CityApiEntity) {
        dataStore.saveCity(city)
            .observeOn(scheduler.ui())
            .subscribeOn(scheduler.io())
            .subscribe({
                view.showMessage(resources.getString(R.string.successfully_added, city.name))
                view.clearSelection()
                view.hideKeyboard()
                view.refreshCities()
                view.hideProgressBar()
            })
            .addTo(disposable)
    }

    fun removeCity(city: CityDbEntity) {
        view.showProgressBar()
        dataStore.removeCity(city)
            .observeOn(scheduler.ui())
            .subscribeOn(scheduler.io())
            .subscribe({
                view.showMessage(resources.getString(R.string.successfully_removed, city.name))
                view.hideKeyboard()
                view.refreshCities()
                view.hideProgressBar()
            })
            .addTo(disposable)
    }
}