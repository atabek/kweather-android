package com.example.kweather.view.detail

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.example.kweather.KweatherApp
import com.example.kweather.R
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity
import com.example.kweather.presenter.DetailsPresenter
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_detail.*
import javax.inject.Inject

class DetailsActivity: AppCompatActivity(), DetailsView {
    @Inject lateinit var presenter: DetailsPresenter
    private val adapter = DetailsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        (application as KweatherApp).component.inject(this)

        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.init(this, CompositeDisposable())

        val cityId = intent.getIntExtra("cityId", -1)
        presenter.requestWeatherForCity(cityId)

        detailsWeatherRecyclerView.adapter = adapter
        detailsWeatherRecyclerView.layoutManager = LinearLayoutManager(this)
        detailsWeatherRecyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        detailsWeatherSwipeRefresh.setOnRefreshListener {
            detailsWeatherSwipeRefresh.isRefreshing = true
            presenter.refreshData(cityId)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.terminate()
    }

    override fun showMessage(messageResId: Int) {
        Snackbar.make(detailsWeatherRecyclerView, messageResId, Snackbar.LENGTH_LONG).show()
    }

    override fun updateData(data: Pair<CityDbEntity, List<WeatherDbEntity>>) {
        setTitle(data.first.name)
        adapter.updateData(data)
    }

    override fun hideProgress() {
        detailsWeatherSwipeRefresh.isRefreshing = false
    }

    private fun setTitle(title: String) {
        supportActionBar?.title = title
    }
}