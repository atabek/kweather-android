package com.example.kweather.view.main

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.example.kweather.KweatherApp
import com.example.kweather.R
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity
import com.example.kweather.presenter.MainPresenter
import com.example.kweather.view.detail.DetailsActivity
import com.example.kweather.view.search.SearchCityActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainView {
    @Inject lateinit var presenter: MainPresenter
    private lateinit var adapter: CityWeatherAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as KweatherApp).component.inject(this)

        fab.setOnClickListener {
            startActivity(Intent(this, SearchCityActivity::class.java))
        }

        presenter.init(this, CompositeDisposable())

        adapter = CityWeatherAdapter(listener)
        cityWeatherRecyclerView.adapter = adapter
        cityWeatherRecyclerView.layoutManager = LinearLayoutManager(this)
        cityWeatherRecyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }

    override fun onResume() {
        super.onResume()
        presenter.requestTodaysWeatherForCities()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_sort -> true
            R.id.action_sort_max_asc,
            R.id.action_sort_max_desc,
            R.id.action_sort_min_asc,
            R.id.action_sort_min_desc,
            R.id.action_sort_humidity_asc,
            R.id.action_sort_humidity_desc -> {
                adapter.sortData(item.itemId)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.terminate()
    }

    override fun showMessage(messageResId: Int) {
        Snackbar.make(fab, messageResId, Snackbar.LENGTH_LONG).show()
    }

    override fun updateCities(cityWeatherPair: List<Pair<CityDbEntity?, WeatherDbEntity?>>) {
        adapter.updateData(cityWeatherPair)
    }

    private val listener = object : CityListener {
        override fun onCityClicked(cityId: Int?) {
            val intent = Intent(this@MainActivity, DetailsActivity::class.java)
            intent.putExtra("cityId", cityId)
            startActivity(intent)
        }
    }
}

interface CityListener {
    fun onCityClicked(cityId: Int?)
}