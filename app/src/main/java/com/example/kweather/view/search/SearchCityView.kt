package com.example.kweather.view.search

import com.example.kweather.model.db.entity.CityDbEntity

interface SearchCityView {
    fun showMessage(message: String)
    fun updateCities(cities: List<CityDbEntity>)
    fun refreshCities()
    fun clearSelection()
    fun hideKeyboard()
    fun showProgressBar()
    fun hideProgressBar()
}