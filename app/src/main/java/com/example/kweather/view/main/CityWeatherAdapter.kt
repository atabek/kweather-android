package com.example.kweather.view.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.kweather.R
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity
import com.example.kweather.model.toHumidity
import com.example.kweather.model.toIcon
import com.example.kweather.model.toTemperature
import com.example.kweather.view.search.DefaultViewHolder

class CityWeatherAdapter(private val listener: CityListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var cityWeatherPair: List<Pair<CityDbEntity?, WeatherDbEntity?>> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_city_weather, parent, false)
        return DefaultViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val cityWeather = cityWeatherPair[position]
        val iconDrawable = holder.itemView.resources.getIdentifier(cityWeather.second?.icon.toIcon(), "drawable", holder.itemView.context.packageName)
        holder.itemView.findViewById<TextView>(R.id.cityWeatherName).text = cityWeather.first?.nameWithCountry()
        holder.itemView.findViewById<ImageView>(R.id.cityWeatherIcon).setImageResource(iconDrawable)
        holder.itemView.findViewById<TextView>(R.id.cityWeatherMaxTemperature).text = cityWeather.second?.max.toTemperature()
        holder.itemView.findViewById<TextView>(R.id.cityWeatherMinTemperature).text = cityWeather.second?.min.toTemperature()
        holder.itemView.findViewById<TextView>(R.id.cityWeatherHumidity).text = cityWeather.second?.humidity.toHumidity()
        holder.itemView.setOnClickListener { listener.onCityClicked(cityWeather.first?.id) }
    }

    override fun getItemCount(): Int {
        return cityWeatherPair.size
    }

    fun updateData(cityWeatherPair: List<Pair<CityDbEntity?, WeatherDbEntity?>>) {
        this.cityWeatherPair = cityWeatherPair
        notifyDataSetChanged()
    }

    fun sortData(sortId: Int) {
        // sort only if we have more than one item in the list
        if (this.cityWeatherPair.size > 1) {
            this.cityWeatherPair = when (sortId) {
                R.id.action_sort_max_asc -> this.cityWeatherPair.sortedWith(compareBy({ it.second?.max }))
                R.id.action_sort_max_desc -> this.cityWeatherPair.sortedWith(compareByDescending({ it.second?.max }))
                R.id.action_sort_min_asc -> this.cityWeatherPair.sortedWith(compareBy({ it.second?.min }))
                R.id.action_sort_min_desc -> this.cityWeatherPair.sortedWith(compareByDescending({ it.second?.min }))
                R.id.action_sort_humidity_asc -> this.cityWeatherPair.sortedWith(compareBy({ it.second?.humidity }))
                R.id.action_sort_humidity_desc -> this.cityWeatherPair.sortedWith(compareByDescending({ it.second?.humidity }))
                else -> this.cityWeatherPair.sortedWith(compareBy({ it.first?.nameWithCountry() }))
            }
            notifyDataSetChanged()
        }
    }
}