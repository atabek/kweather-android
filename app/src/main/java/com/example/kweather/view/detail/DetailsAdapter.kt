package com.example.kweather.view.detail

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.kweather.R
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity
import com.example.kweather.model.toDay
import com.example.kweather.model.toHumidity
import com.example.kweather.model.toIcon
import com.example.kweather.model.toTemperature
import com.example.kweather.view.search.DefaultViewHolder

class DetailsAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var weatherList: List<WeatherDbEntity> = ArrayList()
    private lateinit var city: CityDbEntity

    companion object {
        private const val TYPE_TODAY = 1
        private const val TYPE_OTHER = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = when (viewType) {
            TYPE_TODAY -> LayoutInflater.from(parent.context).inflate(R.layout.item_city_weather_today, parent, false)
            else -> LayoutInflater.from(parent.context).inflate(R.layout.item_city_weather, parent, false)
        }
        return DefaultViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewType = getItemViewType(position)
        val weather = weatherList[position]
        val iconDrawable = holder.itemView.resources.getIdentifier(weather.icon.toIcon(), "drawable", holder.itemView.context.packageName)

        if (viewType == TYPE_TODAY) {
            holder.itemView.findViewById<ImageView>(R.id.detailsTodayIcon).setImageResource(iconDrawable)
            holder.itemView.findViewById<TextView>(R.id.detailsTodayDescription).text = weather.info
            holder.itemView.findViewById<TextView>(R.id.detailsTodayMaxTemp).text = weather.max.toTemperature()
            holder.itemView.findViewById<TextView>(R.id.detailsTodayMinTemp).text = weather.min.toTemperature()
            holder.itemView.findViewById<TextView>(R.id.detailsTodayHumidity).text = weather.humidity.toHumidity()
        } else {
            holder.itemView.findViewById<TextView>(R.id.cityWeatherName).text = weather.date.toDay()
            holder.itemView.findViewById<ImageView>(R.id.cityWeatherIcon).setImageResource(iconDrawable)
            holder.itemView.findViewById<TextView>(R.id.cityWeatherMaxTemperature).text = weather.max.toTemperature()
            holder.itemView.findViewById<TextView>(R.id.cityWeatherMinTemperature).text = weather.min.toTemperature()
            holder.itemView.findViewById<TextView>(R.id.cityWeatherHumidity).text = weather.humidity.toHumidity()
            holder.itemView.findViewById<ImageView>(R.id.cityWeatherArrow).visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return weatherList.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> TYPE_TODAY
            else -> TYPE_OTHER
        }
    }

    fun updateData(data: Pair<CityDbEntity, List<WeatherDbEntity>>) {
        this.city = data.first
        this.weatherList = data.second
        notifyDataSetChanged()
    }
}