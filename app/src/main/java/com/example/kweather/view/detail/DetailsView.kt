package com.example.kweather.view.detail

import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity

interface DetailsView {
    fun showMessage(messageResId: Int)
    fun updateData(data: Pair<CityDbEntity, List<WeatherDbEntity>>)
    fun hideProgress()
}