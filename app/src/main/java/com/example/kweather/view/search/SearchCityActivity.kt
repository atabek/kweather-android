package com.example.kweather.view.search

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.kweather.KweatherApp
import com.example.kweather.R
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.presenter.SearchCityPresenter
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_search.*
import javax.inject.Inject

class SearchCityActivity: AppCompatActivity(), SearchCityView {
    @Inject lateinit var presenter: SearchCityPresenter
    private lateinit var cityAdapter: CityAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        (application as KweatherApp).component.inject(this)

        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.init(this, CompositeDisposable())

        val cityAutocompleteAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, resources.getStringArray(R.array.cities))
        searchTextEdit.threshold = 2
        searchTextEdit.setAdapter(cityAutocompleteAdapter)
        searchTextEdit.onItemClickListener = AdapterView.OnItemClickListener {
            _, view, _, _ -> presenter.searchCity((view as TextView).text.toString())
        }

        cityAdapter = CityAdapter(cityRemovedListener)
        selectedCitiesRecyclerView.adapter = cityAdapter
        selectedCitiesRecyclerView.layoutManager = LinearLayoutManager(this)
        selectedCitiesRecyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        refreshCities()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.terminate()
    }

    override fun showMessage(message: String) {
        Snackbar.make(searchTextEdit, message, Snackbar.LENGTH_LONG).show()
    }

    override fun updateCities(cities: List<CityDbEntity>) {
        cityAdapter.updateCities(cities)
    }

    override fun refreshCities() {
        presenter.getSelectedCities()
    }

    override fun clearSelection() {
        searchTextEdit.setText("")
    }

    override fun hideKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(searchTextEdit.windowToken, 0)
    }

    override fun showProgressBar() {
        searchProgressBar.visibility = View.VISIBLE
        searchProgressBar.isIndeterminate = true
    }

    override fun hideProgressBar() {
        searchProgressBar.visibility = View.VISIBLE
        searchProgressBar.isIndeterminate = false
    }

    private val cityRemovedListener = object : CityRemovedListener {
        override fun onCityRemoved(city: CityDbEntity) {
            AlertDialog.Builder(this@SearchCityActivity)
                .setMessage(getString(R.string.sure_to_delete, city.name))
                .setPositiveButton(R.string.yes) { _, _ -> presenter.removeCity(city) }
                .setNegativeButton(R.string.no, null)
                .create()
                .show()
        }
    }
}

interface CityRemovedListener {
    fun onCityRemoved(city: CityDbEntity)
}