package com.example.kweather.view.search

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.kweather.R
import com.example.kweather.model.db.entity.CityDbEntity

class CityAdapter(private val listener: CityRemovedListener) : RecyclerView.Adapter<DefaultViewHolder>() {
    var citiesList: List<CityDbEntity> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DefaultViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_city, parent, false)
        return DefaultViewHolder(view)
    }

    override fun onBindViewHolder(holder: DefaultViewHolder, position: Int) {
        val city = citiesList[position]
        holder.itemView.findViewById<TextView>(R.id.selectedCityName).text = city.nameWithCountry()
        holder.itemView.findViewById<ImageView>(R.id.selectedCityDelete).setOnClickListener { listener.onCityRemoved(city) }
    }

    override fun getItemCount(): Int {
        return citiesList.size
    }

    fun updateCities(citiesList: List<CityDbEntity>) {
        this.citiesList = citiesList
        notifyDataSetChanged()
    }
}

class DefaultViewHolder(itemView: View?): RecyclerView.ViewHolder(itemView)