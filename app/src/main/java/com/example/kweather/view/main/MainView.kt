package com.example.kweather.view.main

import android.support.annotation.StringRes
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity

interface MainView {
    fun showMessage(@StringRes messageResId: Int)
    fun updateCities(cityWeatherPair: List<Pair<CityDbEntity?, WeatherDbEntity?>>)
}