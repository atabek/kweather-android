package com.example.kweather.model.network.entity

data class WeatherApiEntity(val cnt: Int, val list: List<DailyWeatherApiEntity>, val city: CityApiEntity)