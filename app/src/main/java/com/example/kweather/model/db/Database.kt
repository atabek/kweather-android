package com.example.kweather.model.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity

@Database(entities = arrayOf(CityDbEntity::class, WeatherDbEntity::class), version = 1, exportSchema = false)
abstract class Database : RoomDatabase() {
    abstract val cityDao : CityDao
    abstract val weatherDao: WeatherDao
}
