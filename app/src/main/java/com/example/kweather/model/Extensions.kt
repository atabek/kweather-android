package com.example.kweather.model

import java.util.Calendar
import java.util.Locale
import kotlin.math.roundToInt

fun Float?.toTemperature(): String {
    return String.format("%d°C", this?.roundToInt())
}

fun Int?.toHumidity(): String {
    return when(this) {
        0 -> ""
        else -> String.format("%d%%", this)
    }
}

fun String?.toIcon(): String {
    return when(this) {
        "01d" -> "ic_clear_sky"
        "02d" -> "ic_few_clouds"
        "03d" -> "ic_scattered_clouds"
        "04d" -> "ic_broken_clouds"
        "09d" -> "ic_shower_rain"
        "10d" -> "ic_rain"
        "11d" -> "ic_thunderstorm"
        "13d" -> "ic_snow"
        "50d" -> "ic_mist"
        else -> "ic_clear_sky"
    }
}

fun Long.toDay(): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this * 1000
    return calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault())
}