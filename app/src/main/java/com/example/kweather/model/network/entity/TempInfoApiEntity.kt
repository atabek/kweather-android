package com.example.kweather.model.network.entity

data class TempInfoApiEntity(val min: Float, val max: Float)