package com.example.kweather.model

import com.example.kweather.model.db.Database
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity
import com.example.kweather.model.network.OpenWeatherMapService
import com.example.kweather.model.network.entity.CityApiEntity
import com.example.kweather.model.network.entity.WeatherApiEntity
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import java.util.Calendar

class DataStore(private val networkClient: OpenWeatherMapService, private val dbClient: Database) {

    fun isCityExist(city: String): Single<CityDbEntity> {
        return dbClient
            .cityDao
            .getCityByName(city)
    }

    fun requestWeatherForCity(city: String): Single<WeatherApiEntity> {
        return networkClient
            .getForecast(city)
            .doOnSuccess {
                it -> dbClient.weatherDao.insertWeatherList(Mappers.mapWeatherListFromApiToDb(it))
            }
    }

    fun saveCity(city: CityApiEntity): Completable {
        return Completable.fromCallable {
            dbClient
                .cityDao.
                insertCity(Mappers.mapCityFromApiToDb(city))
        }
    }

    fun removeCity(city: CityDbEntity): Completable {
        return Completable.fromCallable {
            dbClient
                .weatherDao
                .deleteWeatherForCity(city.id)
            }
            .doOnComplete { dbClient.cityDao.deleteCity(city) }
    }

    fun getAllCities(): Single<List<CityDbEntity>> {
        return dbClient
            .cityDao
            .getCities()
    }

    private fun getAllCitiesMap(): Single<HashMap<Int, CityDbEntity>> {
        return dbClient
            .cityDao
            .getCities()
            .map {
                val cityMap = HashMap<Int, CityDbEntity>()
                for (c in it) {
                    cityMap[c.id] = c
                }
                return@map cityMap
            }
    }

    private fun getWeatherForToday(): Single<HashMap<Int, WeatherDbEntity>> {
        val dateStart = Calendar.getInstance()
        dateStart.set(Calendar.HOUR_OF_DAY, 0)
        dateStart.set(Calendar.MINUTE, 0)
        dateStart.set(Calendar.SECOND, 1)

        val dateEnd = Calendar.getInstance()
        dateEnd.set(Calendar.HOUR_OF_DAY, 23)
        dateEnd.set(Calendar.MINUTE, 59)
        dateEnd.set(Calendar.SECOND, 59)

        return dbClient
            .weatherDao
            .getWeatherForDateRange(dateStart.timeInMillis / 1000, dateEnd.timeInMillis / 1000)
            .map {
                val weatherMap = HashMap<Int, WeatherDbEntity>()
                for (w in it) {
                    weatherMap[w.cityId] = w
                }
                return@map weatherMap
            }
    }

    fun getCombinedWeatherForToday(): Single<Pair<HashMap<Int, CityDbEntity>, HashMap<Int, WeatherDbEntity>>> {
        return Single.zip(getAllCitiesMap(), getWeatherForToday(),
            BiFunction { t1: HashMap<Int, CityDbEntity>, t2: HashMap<Int, WeatherDbEntity> -> Pair(t1, t2) })
    }

    fun getWeatherFromTodayForCity(cityId: Int): Single<Pair<CityDbEntity, List<WeatherDbEntity>>> {
        val date = Calendar.getInstance()
        date.set(Calendar.HOUR_OF_DAY, 0)
        date.set(Calendar.MINUTE, 0)
        date.set(Calendar.SECOND, 1)

        return Single.zip(
            dbClient.cityDao.getCityById(cityId),
            dbClient.weatherDao.getWeatherForCityFromToday(date.timeInMillis / 1000, cityId),
            BiFunction { t1: CityDbEntity, t2: List<WeatherDbEntity> -> Pair(t1, t2) })
    }
}