package com.example.kweather.model.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity
import io.reactivex.Single

@Dao
interface CityDao {
    @Query("SELECT * FROM cities ORDER BY city_name")
    fun getCities(): Single<List<CityDbEntity>>

    @Query("SELECT * FROM cities WHERE city_id = :cityId")
    fun getCityById(cityId: Int): Single<CityDbEntity>

    @Query("SELECT * FROM cities WHERE city_name = :city")
    fun getCityByName(city: String): Single<CityDbEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCity(city: CityDbEntity)

    @Delete
    fun deleteCity(city: CityDbEntity)
}

@Dao
interface WeatherDao {
    @Query("SELECT * FROM weather WHERE date > :dateStart AND date < :dateEnd")
    fun getWeatherForDateRange(dateStart: Long, dateEnd: Long): Single<List<WeatherDbEntity>>

    @Query("SELECT * FROM weather WHERE date > :date AND city_id = :cityId")
    fun getWeatherForCityFromToday(date: Long, cityId: Int): Single<List<WeatherDbEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWeatherList(weatherList: List<WeatherDbEntity>)

    @Query("DELETE FROM weather WHERE city_id = :cityId")
    fun deleteWeatherForCity(cityId: Int): Int
}