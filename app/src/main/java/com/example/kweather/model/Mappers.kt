package com.example.kweather.model

import com.example.kweather.model.db.entity.CityDbEntity
import com.example.kweather.model.db.entity.WeatherDbEntity
import com.example.kweather.model.network.entity.CityApiEntity
import com.example.kweather.model.network.entity.WeatherApiEntity

object Mappers {

    fun mapWeatherListFromApiToDb(weatherApiEntity: WeatherApiEntity): List<WeatherDbEntity> {
        val weatherList: MutableList<WeatherDbEntity> = ArrayList()
        val cityId = weatherApiEntity.city.id
        weatherApiEntity.list.forEach {
            weatherList.add(WeatherDbEntity(0, it.dt, it.temp.min, it.temp.max, it.humidity, it.weather[0].main, it.weather[0].description, it.weather[0].icon, cityId))
        }
        return weatherList
    }

    fun mapCityFromApiToDb(city: CityApiEntity): CityDbEntity {
        return CityDbEntity(city.id, city.name, city.country)
    }
}