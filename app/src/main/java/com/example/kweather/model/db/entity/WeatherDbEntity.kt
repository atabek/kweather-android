package com.example.kweather.model.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "weather", indices = arrayOf(Index(value = arrayOf("date", "city_id"), unique = true)))
data class WeatherDbEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "date") val date: Long,
    @ColumnInfo(name = "min") val min: Float,
    @ColumnInfo(name = "max") val max: Float,
    @ColumnInfo(name = "humidity") val humidity: Int,
    @ColumnInfo(name = "info") val info: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "icon") val icon: String,
    @ColumnInfo(name = "city_id") val cityId: Int
)