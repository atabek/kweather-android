package com.example.kweather.model.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "cities")
data class CityDbEntity(
    @PrimaryKey
    @ColumnInfo(name = "city_id") val id: Int,
    @ColumnInfo(name = "city_name") val name: String,
    @ColumnInfo(name = "city_country") val country: String
) {
    fun nameWithCountry() = name.plus(", ").plus(country)
}