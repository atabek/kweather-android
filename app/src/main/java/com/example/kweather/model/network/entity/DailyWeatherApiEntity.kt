package com.example.kweather.model.network.entity

data class DailyWeatherApiEntity(val dt: Long, val temp: TempInfoApiEntity, val humidity: Int, val weather: List<WeatherInfoApiEntity>)