package com.example.kweather.model.network

import com.example.kweather.model.network.entity.WeatherApiEntity
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherMapService {

    @GET("/data/2.5/forecast/daily?units=metric&APPID=87bc30a440d77ac46eda4c5820713770&cnt=7")
    fun getForecast(@Query("q") city: String) : Single<WeatherApiEntity>

}