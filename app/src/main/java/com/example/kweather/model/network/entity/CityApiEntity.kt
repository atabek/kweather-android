package com.example.kweather.model.network.entity

data class CityApiEntity(val id: Int, val name: String, val country: String)