package com.example.kweather.model.network.entity

data class WeatherInfoApiEntity(val id: Int, val main: String, val description: String, val icon: String)