# kweather #

This is sample weather application for android, built using Kotlin, Room, RxJava, Dagger 2, Retrofit, and unit testing.

## Source ##

[OpenWeatherMap](https://openweathermap.org/) was used as the main source for weather information.

## Architecture ##

Model View Presenter (MVP) was selected as a presentation pattern, where we have a passive view and presenter who manipulates data. Initially was thinking to follow Clean Architecture, but decided not to do that as it will require much more efforts. If interested, I have another open source project (in progress, not finished yet) that is using Clean Architecture as the main architecture, you can find it [here](https://github.com/shagalalab/qarejet-android). 

The current app has three screens:

- **Main screen**, where you will see all selected cities with current weather info. Also we can sort the data based on max/min temperatures and humidity.
- **Details screen**, where you can see the weather info for specific city for the coming week. There is an opportunity to force update by pull to refresh.
- And the final screen, **search screen**, where you can add cities to follow. I've added  to the app, so that they will popup up as autocomplete options while typing in search field.

Here are the sample screens:

![main.png](https://bitbucket.org/repo/8zeXAkE/images/1866421916-main.png)
![detail.png](https://bitbucket.org/repo/8zeXAkE/images/2114506293-detail.png)
![search.png](https://bitbucket.org/repo/8zeXAkE/images/3492601758-search.png)
![search_autocomplete.png](https://bitbucket.org/repo/8zeXAkE/images/1682453391-search_autocomplete.png)

## Screen logic ##

- **Main screen** will load all the weather data for given day from local database (Room). When any of the items clicked, we pass the id of the clicked city to details screen.
- When opening **details screen**, we get city id from previous screen, and will retrieve the weather data for given city from local database. If force to update is activated, we will first check whether we have actual data (i.e. we have a weather data for a week), if not we will initiate a network call, and persist the data to local storage (by overriding if needed).
- [List of largest cities](https://en.wikipedia.org/wiki/List_of_largest_cities) are saved as an array resource, and it will be used as autocomplete option when user starts typing in search field. After user selects a city, we initiate a network call, then persist locally weather and city information. We can remove city, which will remove both city and related weather data from local storage.

## Unit tests ##

Partial unit testing is done for DetailsPresenter and MainPresenter

## Limitations ##

- No empty states when no city has been added
- No default city
- No auto refresh when opening detail screen (only available with pull to refresh)
- **Android Studio 3.2 Canary 12** was used for development, for some reasons regular **Android Studio 3.1.2** didn't work well with Kotlin.